const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 手动定义中间件解决跨域
// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*'); // 任意来源
//   res.header('Access-Control-Allow-Methods', '*'); // 允许所有方法进行跨域
//   next();
// });
const cors = require('cors');
app.use(cors()); // 使用cors中间件，允许跨域

app.get('/demo', (req, res) => {
  res.send('get');
});

app.post('/demo', (req, res) => {
  res.send('post');
});

app.delete('/demo', (req, res) => {
  res.send('delete');
});

app.put('/demo', (req, res) => {
  res.send('put');
});
