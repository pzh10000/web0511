const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.get('/demo', (req, res) => {
  // console.log(req.query);
  let { callback } = req.query; // 取得前端传过来的回调函数名
  // console.log(callback); 

  let obj = {
    a: 1,
    b: 2,
  };
  obj = JSON.stringify(obj);
  res.send(`${callback}(${obj})`); // 用回调函数名包裹数据，再返给前端
});
