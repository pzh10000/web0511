const exp = require('constants');
const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 开启静态资源
app.use('/public', express.static(path.join(__dirname, './public')));

// 后端设置中间件接收post参数
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// 渲染get请求页面
app.get('/register_get', (req, res) => {
  res.sendFile(path.join(__dirname, './views/register_get.html'));
});

// 渲染post请求页面
app.get('/register_post', (req, res) => {
  res.sendFile(path.join(__dirname, './views/register_post.html'));
});

// 渲染jq的$.ajax()页面
app.get('/jq', (req, res) => {
  res.sendFile(path.join(__dirname, './views/jq_$ajax.html'));
});

// 实现get请求功能
app.get('/checkuser', (req, res) => {
  // 1、获取输入
  let { username, password } = req.query;

  // 2、非空判断
  if (!username || !password) {
    res.send({
      code: 500,
      msg: '必须输入用户名和密码',
    });
    return;
  }

  // 3、检测用户名是否占用
  if (username === 'demo') {
    res.send({
      code: 500,
      msg: '用户名已占用',
    });
    return;
  }

  res.send({
    code: 200,
    msg: '注册成功',
  });
});

// 实现post请求功能
app.post('/checkuser', (req, res) => {
  // 1、获取输入
  let { username, password } = req.body;

  // 2、非空判断
  if (!username || !password) {
    res.send({
      code: 500,
      msg: '必须输入用户名和密码',
    });
    return;
  }

  // 3、检测用户名是否占用
  if (username === 'demo') {
    res.send({
      code: 500,
      msg: '用户名已占用',
    });
    return;
  }

  res.send({
    code: 200,
    msg: '注册成功',
  });
});
