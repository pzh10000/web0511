const md5 = require("md5"); // 它就是一函数，接收字符串做为参数

console.log(md5("a")); // 0cc175b9c0f1b6a831c399e269772661
console.log(md5("a"));
console.log(md5("a"));
console.log(md5("东边一个汉，西边一个汉，座个小车逛咸阳"));

// 密码加密的原理：先设置一个密钥，将用户的密码和这个密钥一起加密，填到数据库中。
// 当用户再登录时，再用用户的密码和密钥一起加密，和数据库中的比对，比对上就成功。

let yaoshi = "sdfdsadsaffwe";
let str = "abc";

console.log(md5(yaoshi + str)); // b28f396bf4c775548a610c95db941adc
console.log(md5(yaoshi + 'abc')); // b28f396bf4c775548a610c95db941adc
