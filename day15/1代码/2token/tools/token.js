const jwt = require("jsonwebtoken");
const { secret } = require("../config"); // 密钥

// 加密，接收一个对象，返回一个token字符串
function sign(obj, t = 1) {
  return jwt.sign(obj, secret, { expiresIn: t * 24 * 60 * 60 });
}

// 解密，传入token字符串，如果成功，返回对象，如果失败，返回false
function verify(token) {
  try {
    return jwt.verify(token, secret);
  } catch (error) {
    return false;
  }
}

module.exports = {
  sign,
  verify,
};
