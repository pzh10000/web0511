const jwt = require("jsonwebtoken");
let secret = "sdfkldsajf;lkdsa;ljdsag;lk";

// 需要被加密的对象
let obj = {
  name: "zs",
  age: 3,
  job: "前端开发",
};

// 加密：jwt.sign(data, 密钥 [,option对象])   加密之后，会返回一个token字符串
// 验证：jwt.verify( token, 密钥 )        接收token字符串，如果解密成功，返回一个对象，解密失败报错

// 加密对象，返回一个token字符串
let str = jwt.sign(obj, secret, { expiresIn: 24 * 60 * 60 });
console.log(str); // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoienMiLCJhZ2UiOjMsImpvYiI6IuWJjeerr-W8gOWPkSIsImlhdCI6MTY1Mzk2NDUwNywiZXhwIjoxNjU0MDUwOTA3fQ.pjWLoO93GjxpidF6_rUaxF1qLyO-tjheJbexDi93cNM

// 解密token，成功返回对象，失败报错
console.log(jwt.verify(str, secret));
