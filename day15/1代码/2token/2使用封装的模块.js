const { sign, verify } = require("./tools/token");

// console.log(sign);
// console.log(verify);

let obj = {
  name: "睡不醒",
  age: 12,
  sex: "男",
};

// 加密
let token = sign(obj);
console.log(token);

// 解密
console.log(verify(token + 1));
