﻿练习1：有已知以下购物车数据，每一个都选中了，则打印true（选中了），只要有一个没有选中则打印false(未选中了)

var cartGoods = [
    {
        goodsname:'小米10',
        price:5000,
        isChecked:true // 代表是否选中了
    },
    {
        goodsname:'苹果10',
        price:3000,
        isChecked:false // 代表是否选中了
    }
]





练习2：有已知以下某公司员工薪金信息，把每一位员工的salary薪金信息 加 2000，并返回新数组，注意：不影响原数组
var persons = [
    {
        username:'张飞',
        sex:'男',
        salary:50000
    },
    {
        username:'关羽',
        sex:'男',
        salary:60000
    }
]


