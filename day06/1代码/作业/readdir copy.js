const fs = require('fs');
const path = require('path');

let d = '';

function readdir(dir) {
  let arr = fs.readdirSync(dir); // [ '444.txt', 'a', 'b' ]
  arr.forEach((item) => {
    let url = path.join(dir, item);
    if (fs.statSync(url).isDirectory()) {
      // 判断如果是文件夹，则再调一次
      readdir(url);
    } else {
      // console.log(url);
      d += url + '\n';
    }
  });
  return d;
}

let url = path.join(__dirname, './demo');
// console.log(url);
console.log(readdir(url));
