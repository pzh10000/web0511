const fs = require('fs');
const path = require('path');

function readdir(dir) {
  let arr = fs.readdirSync(dir); // 读取第一层   [ '444.txt', 'a', 'b' ]
  arr.forEach((item) => {
    let url = path.join(dir, item); // 拼出一个路径
    if (fs.statSync(url).isDirectory()) {
      // 判断如果是文件夹，则再递归调一次
      readdir(url);
    } else {
      // 如果是文件，则放到函数的一个自定义的属性中
      if (readdir.files) {
        readdir.files.push(url);
      } else {
        readdir.files = [url];
      }
    }
  });
  return readdir.files;
}

let url = path.join(__dirname, './demo');
// console.log(url);
console.log(readdir(url));
