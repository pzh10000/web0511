const http = require('http');
let app = http.createServer();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  console.log('请求方法', req.method); // 请求方法
  console.log('请求地址', req.url); // 请求地址
  console.log('------------------');

  // 写在响应的前面，设置文件类型
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' });
  res.end('<h1>二狗子，回家吃饭了</h1>');
});
