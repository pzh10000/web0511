const http = require('http');
let app = http.createServer();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  let url = req.url; // 请求的地址
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' });

  if (url.startsWith('/login')) {
    // 登录
    res.end('<h1>登录</h1>');
  } else if (url.startsWith('/register')) {
    // 注册
    res.end('<h1>注册</h1>');
  } else {
    // 404
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf8' });
    res.end('404');
  }
});
