// 1、引入http模块
const http = require('http');
// console.log(http); // {}  它下面有很多的属性和方法，createServer即创建服务器

let app = http.createServer(); // 创建一个服务器

// 服务器监听端口，第一个是端口号，第二个是一个回调函数，如果服务器开启成功，回调函数执行（回调函数可写可不写，这个回调函数是给开发者看的）
app.listen('3000', () => {
  console.log('3000端口开启成功');
});

// 当服务器开启成功之后，它就监听用户的request请求，当一个请求进来，这个回调函数就会执行
// 这个回调函数有两个参数，分别是请求request和响应response
// 有请求，就一定要有响应，用res.end('响应的内容')

let n = 0;
app.on('request', (req, res) => {
  // console.log('有一个请求进来了');
  n++;
  console.log('第' + n + '个请求进来了');

  res.end('welcome to node');
});
