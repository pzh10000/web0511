const http = require('http');
let app = http.createServer();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const fs = require('fs');
const path = require('path');

app.on('request', (req, res) => {
  let url = req.url; // 请求的地址
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' });

  if (url.startsWith('/login')) {
    // 登录
    let fileUrl = path.join(__dirname, './views/login.html');
    let data = fs.readFileSync(fileUrl, 'utf-8');
    res.end(data);
  } else if (url.startsWith('/register')) {
    // 注册
    let fileUrl = path.join(__dirname, './views/register.html');
    let data = fs.readFileSync(fileUrl);
    res.end(data);
  } else {
    // 404
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf8' });
    res.end('404');
  }
});
