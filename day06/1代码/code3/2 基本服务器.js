const http = require('http');
let app = http.createServer();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  res.end('hello');
});
