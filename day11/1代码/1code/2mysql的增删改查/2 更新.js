// 1、下载  npm i mysql

// 2、引包
const mysql = require('mysql');

// 3、创建连接
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306, // 默认3306
  database: 'xsk',
});

// 4、使用connectObj.query(sql语句, (err, result)=>{})

let sql = `UPDATE xsb SET xm='周扒皮',sex='男' WHERE xh='9521103'`;
connectObj.query(sql, (err, result) => {
  // console.log(err); // 错误对象，如果连接信息错误，或者sql错误，错误对象才有值
  // console.log(result); // 返回的信息

  if (err) {
    console.log('查询错误');
    return;
  }
  // 如果有受影响的行，则证明更新成功
  if (result.affectedRows) {
    console.log('更新成功');
  }
});
