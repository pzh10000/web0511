// 1、下载  npm i mysql

// 2、引包
const mysql = require('mysql');

// 3、创建连接
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306, // 默认3306
  database: 'xsk',
});

function Query(sql = '') {
  return new Promise((resolve, reject) => {
    connectObj.query(sql, (err, result) => {
      if (!err) {
        resolve([null, result]); // 成功
      } else {
        resolve([err]); // 失败
      }
    });
  });
}

let sql = `SELECT xh,xm FROM xsb1`;
async function auto() {
  // let arr = await Query(sql);
  // console.log(arr);

  // 返回一个对象，可以解构
  let [err, result] = await Query(sql);
  if (err) {
    console.log('获取错误');
    return;
  }

  console.log(result);
}
auto();
