var express = require('express');
var router = express.Router();

let Query = require('../tools/db'); // 引入数据库查询工具

router.get('/', function (req, res, next) {
  res.send('首页');
});

// 渲染注册页面路由
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 注册功能路由
router.post('/register', async function (req, res, next) {
  let { username, password } = req.body; // 获取用户输入

  // 1、必要的判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 2、查找数据库是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username = '${username}'`;
  let [err1, result1] = await Query(sql1);
  if (err1) {
    next('服务器内部误');
    return;
  }
  if (result1.length) {
    next('用户名已占用');
    return;
  }

  // 3、将用户名和密码添加到数据库中
  let sql2 = `INSERT INTO user (username, password) values ('${username}', '${password}')`;
  let [err2, result2] = await Query(sql2);
  if (err2) {
    next('服务器内部误');
    return;
  }
  if (result2.affectedRows) {
    res.send({
      code: 200,
      msg: '注册成功',
    });
  }
});

module.exports = router;
