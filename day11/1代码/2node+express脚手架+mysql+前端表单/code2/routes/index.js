var express = require('express');
var router = express.Router();

const mysql = require('mysql'); // 引入mysql
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'xiaou2',
});

router.get('/', function (req, res, next) {
  res.send('首页');
});

// 渲染注册页面路由
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 注册功能路由
router.post('/register', function (req, res, next) {
  let { username, password } = req.body; // 获取用户输入

  // 1、必要的判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 2、查找数据库是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username = '${username}'`;
  connectObj.query(sql1, (err, result) => {
    if (err) {
      next('服务器内部错误');
      return;
    }

    // 读取数据，不论有不有数据，都返回一个数组
    if (result.length) {
      next('用户名已占用');
      return;
    }

    // 3、向数据库添加数据
    let sql2 = `INSERT INTO user (username, password) values ('${username}', '${password}')`;
    connectObj.query(sql2, (err, result) => {
      if (err) {
        next('服务器内部错误');
        return;
      }

      if (result.affectedRows) {
        res.send({
          code: 200,
          msg: '注册成功',
        });
      }
    });
  });
});

module.exports = router;
