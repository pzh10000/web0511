var express = require('express');
var router = express.Router();
const Query = require('../tools/db');

// 注册接口
router.post('/register', async (req, res, next) => {
  let { username, password } = req.body;
  // console.log(username, password);

  // 1、必要判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 2、查找数据库是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username = '${username}'`;
  let [err1, res1] = await Query(sql1);
  if (err1) {
    next('服务器内部错误');
    return;
  }
  if (res1.length) {
    next('该用户名已占用');
    return;
  }

  // 添加到数据库
  // 3、将用户名和密码添加到数据库中
  let sql2 = `INSERT INTO user (username, password) values ('${username}', '${password}')`;
  let [err2, result2] = await Query(sql2);
  if (err2) {
    next('服务器内部误');
    return;
  }
  if (result2.affectedRows) {
    res.send({
      code: 200,
      msg: '注册成功',
    });
  }
});

// 登录接口
router.post('/login', async (req, res, next) => {
  let { username, password } = req.body;
  // console.log(username, password);

  // 1、必要判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 2、查找数据库是否有这个用户
  let sql = `SELECT * FROM user WHERE username='${username}' AND password='${password}'`;
  let [err, result] = await Query(sql);
  if (err) {
    next('服务器内部错误');
    return;
  }
  if (!result.length) {
    next('用户名密码错误');
    return;
  }

  res.send({
    code: 200,
    msg: '登录成功',
  });
});

module.exports = router;
