function Http({ url = '', type = 'get', data = {} }) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url,
      type,
      data,
      success(result) {
        resolve([null, result]);
      },
      error(err) {
        resolve([err]);
      },
    });
  });
}
