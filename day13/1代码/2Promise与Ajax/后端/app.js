const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cors = require('cors');
app.use(cors());

app.get('/demo', (req, res) => {
  res.send(req.query);
  // res.send('ok');
});
