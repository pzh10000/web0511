const mysql = require('mysql');

// 数据库连接配置
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306, // 默认3306
  database: 'xiaou2',
});

function Query(sql = '') {
  return new Promise((resolve, reject) => {
    connectObj.query(sql, (err, result) => {
      if (!err) {
        resolve([null, result]); // 成功
      } else {
        resolve([err]); // 失败
      }
    });
  });
}

module.exports = Query;
