const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 开放静态资源
app.use('/static', express.static(path.join(__dirname, './static')));

// 接收post请求参数的中间件
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const cookieSession = require('cookie-session'); // 引入cookieSession中间件
var svgCaptcha = require('svg-captcha'); // 引入验证码模块
// 使用cookieSession中间件
app.use(
  cookieSession({
    keys: ['sdafdsafdsaf', '45w65', 'fdsfdsdsfds'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

app.get('/captcha', function (req, res) {
  var captcha = svgCaptcha.create({
    size: 4, // 字符长度，默认4
    ignoreChars: '0oO1iILl', // 被忽略的文字
    noise: 3, // 干扰线的条数
    color: true, // 颜色是否彩色
    background: '#cc9966', // 背景色
  });
  // console.log(captcha.text); // 验证码的内容，要结合session一起用
  req.session.captcha = captcha.text; // 将验证码放入session中

  res.type('svg');
  res.status(200);
  res.send(captcha.data);
});

// 引入模块化
let login = require('./routes/login');
let register = require('./routes/register');
app.use(login);
app.use(register);

// 配置404错误
app.use((req, res) => {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

// 配置逻辑错误处理
app.use((err, req, res, next) => {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});
