/*
Navicat MySQL Data Transfer

Source Server         : 随机起
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : xsk

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2021-04-24 14:29:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cjb
-- ----------------------------
DROP TABLE IF EXISTS `cjb`;
CREATE TABLE `cjb` (
  `xh` varchar(20) NOT NULL DEFAULT '',
  `kch` varchar(20) NOT NULL DEFAULT '',
  `cj` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cjb
-- ----------------------------
INSERT INTO `cjb` VALUES ('9512101', 'c01', '97');
INSERT INTO `cjb` VALUES ('9512101', 'c02', '93');
INSERT INTO `cjb` VALUES ('9512102', 'c02', '85');
INSERT INTO `cjb` VALUES ('9512102', 'c04', '73');
INSERT INTO `cjb` VALUES ('9521102', 'c01', '87');
INSERT INTO `cjb` VALUES ('9521102', 'c02', '80');
INSERT INTO `cjb` VALUES ('9521102', 'c04', '97');
INSERT INTO `cjb` VALUES ('9521102', 'c05', '55');
INSERT INTO `cjb` VALUES ('9521103', 'c02', '73');
INSERT INTO `cjb` VALUES ('9521103', 'c06', '61');
INSERT INTO `cjb` VALUES ('9531101', 'c01', '85');
INSERT INTO `cjb` VALUES ('9531101', 'c05', '100');
INSERT INTO `cjb` VALUES ('9531102', 'c05', '90');

-- ----------------------------
-- Table structure for kcb
-- ----------------------------
DROP TABLE IF EXISTS `kcb`;
CREATE TABLE `kcb` (
  `kch` varchar(20) NOT NULL,
  `kcm` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kcb
-- ----------------------------
INSERT INTO `kcb` VALUES ('c01', '计算机文化');
INSERT INTO `kcb` VALUES ('c02', 'vb');
INSERT INTO `kcb` VALUES ('c03', '计算机网络');
INSERT INTO `kcb` VALUES ('c04', '数据库基础');
INSERT INTO `kcb` VALUES ('c05', '高等数学');
INSERT INTO `kcb` VALUES ('c06', '数据结构');

-- ----------------------------
-- Table structure for xsb
-- ----------------------------
DROP TABLE IF EXISTS `xsb`;
CREATE TABLE `xsb` (
  `xh` varchar(20) NOT NULL,
  `xm` varchar(20) NOT NULL,
  `sex` char(2) DEFAULT '男',
  `age` int(11) DEFAULT NULL,
  `szx` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xsb
-- ----------------------------
INSERT INTO `xsb` VALUES ('9512101', '李勇', '男', '19', '计算机系');
INSERT INTO `xsb` VALUES ('9512102', '刘晨', '男', '20', '计算机系');
INSERT INTO `xsb` VALUES ('9512103', '王敏', '女', '20', '计算机系');
INSERT INTO `xsb` VALUES ('9521101', '张力', '男', '22', '信息系');
INSERT INTO `xsb` VALUES ('9521102', '吴宾', '女', '21', '信息系');
INSERT INTO `xsb` VALUES ('9521103', '张海', '男', '20', '信息系');
INSERT INTO `xsb` VALUES ('9531101', '钱小平', '女', '18', '数学系');
INSERT INTO `xsb` VALUES ('9531102', '王大力', '男', '19', '数学系');
INSERT INTO `xsb` VALUES ('9527', '周星星', '男', '25', '表演系');
