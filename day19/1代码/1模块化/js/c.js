// 一个js文件里面，只能有一个默认暴露
// 默认暴露的时候，可以给名，也可以不给名
export default function (min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let a1 = 12;
let a2 = 22;

export { a1, a2 };
