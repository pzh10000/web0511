console.log("我是入口文件");

// ------------------------
// 引入暴露的数据
import { a, b, c } from "./a.js";
console.log(a, b, c);

// ------------------------
// 引入暴露的数据
import { a1, a2 } from "./b.js";
console.log(a1, a2);

// ------------------------
// 接收默认暴露的同时，还可以接收单个暴露
// 注意：默认暴露的必须写在最前面，可以用任何名
// 如果变量名已经被占用，则可以用 as 改名
import getRandom, { a1 as aa1, a2 as aa2 } from "./c.js";
console.log(getRandom);
console.log(aa1, aa2);
