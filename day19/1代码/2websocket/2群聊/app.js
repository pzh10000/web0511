const express = require("express");
let app = express();
let server = app.listen(3000); // 端口开启成功之后，会返回一个http服务器对象
let path = require("path");

let io = require("socket.io")(server); // 引入socket.io，它返回一个函数，我们调用函数并传入http服务器，返回io服务器对象

io.on("connect", (socket) => {
  // console.log(socket); // 连接进来的哪个客户端对象
  // console.log(io.sockets); // 代表所有连接进来的用户
  // console.log("有一个连接进来了");

  socket.on("qth", (msg) => {
    // 向所有用户广播
    io.sockets.emit("htq", msg);
  });
});

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./index.html"));
});
