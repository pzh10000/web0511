const express = require("express");
let app = express();
let server = app.listen(3000); // 端口开启成功之后，会返回一个http服务器对象

let io = require("socket.io")(server); // 引入socket.io，它返回一个函数，我们调用函数并传入http服务器，返回io服务器对象
// console.log(io); // 它是一个io服务器，它是一个对象，它下面有很多的属性和方法

// 只要前端有连接进来，这个函数就会执行
io.on("connect", (socket) => {
  // console.log(socket); // 连接进来的哪个客户端对象
  // console.log(io.sockets); // 代表所有连接进来的用户
  // console.log("有一个连接进来了");


  // 接收前端的请求，msg为前端发给我的信息
  socket.on("qth", (msg) => {
    console.log(msg);

    // 向前端发起请求
    socket.emit("htq", "小甜甜，醒醒吧");
  });
});
