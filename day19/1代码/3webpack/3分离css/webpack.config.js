const path = require("path");
// 引入分离css模块
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  context: path.join(__dirname, "./src"), // 配置入口路径（不是必须的），以后的相对路径都相对于这个文件夹
  entry: "./main.js", // 入口文件，这个文件中引入别的js、css、less、字体图标等
  output: {
    path: path.join(__dirname, "./dist"), // 出口目录，必须是绝对路径
    filename: "bundle.js", // 打包以后的文件名
  },
  mode: "development", // development开发环境  production生产环境
  module: {
    rules: [
      // 加载器的使用规则
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          // 从一个已存在的 loader 中，创建一个提取(extract) loader。
          fallback: "style-loader",
          use: "css-loader", // loader被用于将资源转换成一个CSS单独文件
        }),
      },
    ],
  },
  plugins: [
    // 插件的配置，插件需要new
    new ExtractTextPlugin("./css/style.css"), // 这个参数即为分离后的css放到哪里，放到这个打包以后的dist/css/style.css，在页面中需要link引入一下
  ],
};
