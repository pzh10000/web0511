const path = require("path");

module.exports = {
  context: path.join(__dirname, "./src"), // 配置入口路径（不是必须的），以后的相对路径都相对于这个文件夹
  entry: "./main.js", // 入口文件，这个文件中引入别的js、css、less、字体图标等
  output: {
    path: path.join(__dirname, "./dist"), // 出口目录，必须是绝对路径
    filename: "bundle.js", // 打包以后的文件名
  },
  mode: "development", // development开发环境  production生产环境
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
