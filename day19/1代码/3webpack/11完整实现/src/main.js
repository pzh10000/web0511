import { a, b } from "./js/a.js";
console.log(a, b);

// 引入css
import "./css/index.css";
// 引入less
import "./less/ab.less";
// 引入字体图标
import "./myfont/iconfont.css";

// 使用jquery，在哪个文件中使用，就在哪个文件中引入
import $ from "jquery";
$(".bg").click(function () {
  alert("哥们，点我了");
});
