const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieParser = require('cookie-parser'); // 引入cookie-parser中间件
app.use(cookieParser()); // 使用cookie-parser中间件，全局开启cookie功用

// 登录接口，设置cookie
app.get('/login', (req, res) => {
  // 此处应该做登录的逻辑判断
  res.cookie('username', '王二小', { maxAge: 24 * 60 * 60 * 1000 }); // 一天以后过期

  res.send('登录');
});

// 欢迎页
app.get('/welcome', (req, res) => {
  let { username } = req.cookies;
  // console.log(username);
  if (username) {
    res.send('欢迎' + username + '回家');
  } else {
    res.send('欢迎游客回家');
  }
});

// 编缉页（需要用户权限）
app.get('/user/edit', (req, res) => {
  let { username } = req.cookies;
  if (username) {
    res.send(username + '你好，可以编缉');
  } else {
    res.send('你还没有登录，请到登录');
  }
});
