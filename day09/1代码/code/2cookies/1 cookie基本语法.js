const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieParser = require('cookie-parser'); // 引入cookie-parser中间件
app.use(cookieParser()); // 使用cookie-parser中间件，全局开启cookie功用

// demo1被访问时，设置cookie，后端生存cookie，存在浏览器中
app.get('/demo1', (req, res) => {
  // res.cookie(key, value [,option])
  res.cookie('name', 'zs');
  res.cookie('age', 3, { maxAge: 24 * 60 * 60 * 1000 }); // 一天以后过期
  res.cookie('obj', { a: 1, b: 2 }, { maxAge: 24 * 60 * 60 * 1000 });

  res.send('后端 设置cookie');
});

// demo2被访问时，会待上cookie
// 前端访问后端时，会携带cookie，后端就可以在req.cookies中取得
app.get('/demo2', (req, res) => {
  console.log(req.cookies); // { name: 'zs', age: '3', obj: { a: 1, b: 2 } }
  res.send('后端 获取cookie');
});

app.get('/a/b/c', (req, res) => {
  let { name, age, obj } = req.cookies;
  console.log(name, age, obj);
  res.send('cookie在整个项目中有效');
});
