const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});
const fs = require('fs');
const path = require('path');
const formidable = require('formidable'); // 引入上传中间件
const timeStamp = require('time-stamp'); // 时间模块

// 上传路由
app.post('/up', (req, res, next) => {
  // 实例化一个上传表单功能
  const form = formidable({
    uploadDir: path.join(__dirname, './tempdir'), // 临时的路径存放地址，要绝对地址
  });

  form.parse(req, (err, fields, files) => {
    if (err) {
      res.send('服务器内部错误');
      return;
    }

    // console.log(err); // 错误信息
    // console.log(fields); // 普通的表单元素数据
    // console.log(files); // 文件数据

    // ---------------------------
    // 处理具体的业务。比如：文件名的重命名，把文件最终放在的位置是哪里
    // 改名，利用时间模块
    let oldpath = files.f.path; // 原来文件的路径
    // 新文件名：时间+6位数的随机数
    let newpath =
      timeStamp('YYYYMMDDHHmmss') + Math.random().toString().slice(2, 8);
    newpath += path.extname(files.f.name); // 拼上后缀
    newpath = path.join(__dirname, './uploads', newpath); // 新的文件路径

    fs.renameSync(oldpath, newpath); // 改名

    res.send('文件上传。。。');
  });
});
