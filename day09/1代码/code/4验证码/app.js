const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');
var svgCaptcha = require('svg-captcha'); // 引入验证码模块
const cookieSession = require('cookie-session'); // 引入cookieSession中间件
app.use(
  cookieSession({
    keys: ['dsagfsfwqerew', 'dsafdsgre', 'jiutfdgfds'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

// 渲染页面
app.get('/demo', (req, res) => {
  res.sendFile(path.join(__dirname, './views/ab.html'));
});

// 验证码路由
app.get('/captcha', function (req, res) {
  var captcha = svgCaptcha.create({
    size: 4, // 字符长度，默认4
    ignoreChars: '0oO1iILl', // 被忽略的文字
    noise: 3, // 干扰线的条数
    // color: true, // 颜色是否彩色
    // background: '#cc9966', // 背景色
  });

  req.session.captcha = captcha.text; // 关键点，将验证码放到session中，后面同用户输入的进行对比

  res.type('svg');
  res.status(200);
  res.send(captcha.data);
});

// 检测输入的验证码和图片上的验证码是否一致
app.get('/login', (req, res) => {
  // console.log(req.query); // { code: 'abc' }
  let { code } = req.query; // 用户输入的

  // console.log(code, '用户输入的');
  // console.log(req.session.captcha, '本地session存储的');
  if (code.toLowerCase() === req.session.captcha.toLowerCase()) {
    res.send('验证码验证成功');
  } else {
    res.send('验证失败');
  }
});
