const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.set('view engine', 'ejs'); // 设置ejs模板引擎

let username = '猴子';
let age = 3;
let sex = '男';
let isPerson = false;
let arr = ['吃香蕉', '抢苹果', '进水帘洞'];

app.get('/index', (req, res) => {
  // res.render('模板名称' [, data ])
  res.render('index', {
    username,
    age,
    sex,
    isPerson,
    arr,
  });
});

app.get('/list', (req, res) => {
  res.render('list');
});
