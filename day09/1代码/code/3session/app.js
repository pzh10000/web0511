const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieSession = require('cookie-session'); // 引入cookieSession中间件
app.use(
  cookieSession({
    // name: 'xiaoer', // sessionid的名称，默认可以省略
    keys: ['dsagfsfwqerew', 'dsafdsgre', 'jiutfdgfds'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

// 设置session，在req中
app.get('/demo1', (req, res) => {
  // 做一些服务器验证，比如：用户名和密码是否匹配，如果匹配了，才设置
  req.session.name = 'zs';
  req.session.age = 3;
  req.session.obj = {
    a: 1,
    b: 2,
  };
  res.send('设置session');
});

// 获取session，在req中
app.get('/demo2', (req, res) => {
  console.log(req.session); // 一个对象，可以解构
  res.send('获取session');
});
