const express = require('express');
const app = express.Router();
const fs = require('fs');
const path = require('path');

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/login.html'));
});

app.post('/login', (req, res, next) => {
  // 取得用户输入
  let { tel, pass } = req.body;

  // 1、非空判断
  if (!tel || !pass) {
    next('请输入必填项');
    return;
  }

  // 2、取得json中的数据
  let filePath = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(filePath, 'utf-8');
  data = JSON.parse(data);
  // console.log(data);

  // 3、登录逻辑
  let o = data.find((item) => item.username === tel && item.password === pass);
  if (o) {
    // 登录成功
    res.send({
      code: 200,
      msg: '登录成功',
    });
  } else {
    next('用户和密码不匹配');
    return;
  }
});

module.exports = app;
