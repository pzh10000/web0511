const express = require('express');
const app = express.Router();
const fs = require('fs');
const path = require('path');

let timeStamp = require('time-stamp');

app.get('/register', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/register.html'));
});

app.post('/register', (req, res, next) => {
  let { tel, pass, pass2 } = req.body;

  // 1、非空判断
  if (!tel || !pass || !pass2) {
    next('请输入必填项');
    return;
  }
  if (pass !== pass2) {
    next('两次密码必须一致');
    return;
  }

  // 2、取得json中的数据
  let filePath = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(filePath, 'utf-8');
  data = JSON.parse(data);

  // 3、注册逻辑
  let o = data.findIndex((item) => item.username === tel);
  if (o === -1) {
    // 可以注册
    data.push({
      username: tel,
      password: pass,
      time: timeStamp('YYYY年MM月DD日 HH:mm:ss'),
    });
    fs.writeFileSync(filePath, JSON.stringify(data));
    res.send({
      code: 200,
      msg: '注册成功',
    });
  } else {
    // 用户名已占用
    next('用户名已占用');
    return;
  }
});

module.exports = app;
