const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 开放静态资源
app.use('/static', express.static(path.join(__dirname, './static')));

// 接收post请求参数的中间件
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// 引入模块化
let login = require('./routes/login');
let register = require('./routes/register');
app.use(login);
app.use(register);

// 配置404错误
app.use((req, res) => {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

// 配置逻辑错误处理
app.use((err, req, res, next) => {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});
