const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 引入模块化路由
let login = require('./route/login');
let register = require('./route/register');
// 使用
app.use(login);
app.use(register);
