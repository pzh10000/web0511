const express = require('express');
let api = express.Router();

const fs = require('fs');
const path = require('path');

// 渲染页面
api.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/login.html'));
});

// 实现登录功能
api.get('/dologin', (req, res) => {
  // 1、接收用户传入的参数
  let { username, password } = req.query;

  // 2、进行非空判断
  if (!username || !password) {
    res.send('请填写完整数据');
    return;
  }

  // 3、读取json文件
  let fileUrl = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(fileUrl, 'utf-8');
  data = JSON.parse(data);
  // console.log(data);

  // 4、进行登录逻辑判断
  let o = data.find(
    (item) => item.username === username && item.password === password
  );
  if (o) {
    res.send('登录成功');
  } else {
    res.send('用户名密码不匹配');
  }
});

module.exports = api;
