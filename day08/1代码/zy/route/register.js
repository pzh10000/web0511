const express = require('express');
let api = express.Router();

const fs = require('fs');
const path = require('path');

// 渲染页面
api.get('/register', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/register.html'));
});

// 实现注册功能
api.get('/doregister', (req, res) => {
  // 1、接收用户传入的参数
  let { username, password } = req.query;

  // 2、进行非空判断
  if (!username || !password) {
    res.send('请填写完整数据');
    return;
  }

  // 3、读取json文件
  let fileUrl = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(fileUrl, 'utf-8');
  data = JSON.parse(data);
  // console.log(data);

  // 4、进行逻辑判断
  let o = data.findIndex((item) => item.username === username);
  if (o === -1) {
    // 可以注册
    data.push({
      username,
      password,
    });

    fs.writeFileSync(fileUrl, JSON.stringify(data));

    res.send('注册成功');
  } else {
    // 用户名已占用
    res.send('此用户名太火，请换一个');
  }
});

module.exports = api;
