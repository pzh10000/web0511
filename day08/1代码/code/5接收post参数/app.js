const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 接收前端的查询字符串参数
app.use(express.urlencoded({ extended: false }));
// 接收前端的json对象
app.use(express.json());

app.post('/demo1', (req, res) => {
  console.log(req.body); // { uname: 'zs', age: '3', sex: '男' }
  res.send(req.body);
});

app.post('/demo2', (req, res) => {
  console.log(req.body); // { a: 1, b: 2 }
  res.send(req.body);
});
