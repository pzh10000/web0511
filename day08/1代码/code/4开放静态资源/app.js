const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 不带前缀的写法，访问时也不带前缀
// http://localhost:3000/img/erha1.jpg
// app.use(express.static(path.join(__dirname, './public'))); // 文件夹路径，尽量使用绝对地址，因为底层是在进行文件的读写操作

// 带前缀
// 访问地址：http://localhost:3000/public/img/erha1.jpg
app.use('/public', express.static(path.join(__dirname, './public')));

app.get('/demo', (req, res) => {
  res.sendFile(path.join(__dirname, './views/index.html'));
});
