const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let timeStamp = require('time-stamp'); // 引入时间模块

// 定义获取前时间中间件
app.use((req, res, next) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  // console.log(time);
  req.time = time; // 将时间放在了请求对象上，后面的路由都可以获取到这个请求对象上的属性

  next();
});

app.get('/login', (req, res) => {
  res.send(`get-login，当前时间是：${req.time}`);
});

app.post('/login', (req, res) => {
  res.send(`post-login--${req.time}`);
});

app.get('/register', (req, res) => {
  res.send(`get-register--${req.time}`);
});

app.post('/register', (req, res) => {
  res.send(`post-register--${req.time}`);
});
