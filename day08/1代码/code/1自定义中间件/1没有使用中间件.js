const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let timeStamp = require('time-stamp'); // 引入时间模块

// 案例：无论什么请求及何种路径，都要获取当前的请求时间

app.get('/login', (req, res) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  // console.log(time);
  res.send(`get-login，当前时间是：${time}`);
});

app.post('/login', (req, res) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  res.send(`post-login--${time}`);
});

app.get('/register', (req, res) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  res.send(`get-register--${time}`);
});

app.post('/register', (req, res) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  res.send(`post-register--${time}`);
});
