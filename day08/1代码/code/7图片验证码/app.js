const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

var svgCaptcha = require('svg-captcha'); // 引入模块

// http://localhost:3000/captcha  作为img标签的src属性

app.get('/captcha', function (req, res) {
  // var captcha = svgCaptcha.create(); // 创建时走默认

  var captcha = svgCaptcha.create({
    size: 4, // 字符长度，默认4
    ignoreChars: '0oO1iILl', // 被忽略的文字
    noise: 3, // 干扰线的条数
    color: true, // 颜色是否彩色
    background: '#cc9966', // 背景色
  });
  console.log(captcha.text); // 验证码的内容，要结合session一起用

  res.type('svg');
  res.status(200);
  res.send(captcha.data);
});
