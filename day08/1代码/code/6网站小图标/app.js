const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

var favicon = require('serve-favicon');
var path = require('path');

// 使用网站小图标
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.get('/', (req, res) => {
  res.send('首页');
});
