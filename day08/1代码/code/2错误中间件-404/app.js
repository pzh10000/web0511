const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

app.get('/demo1', (req, res) => {
  res.send('我是demo1');
});

app.get('/demo2', (req, res) => {
  res.send('我是demo2');
});

// 404错误，放在所有路由的最后
// 错误中间件一般不加前缀，也不用next下一步，同时要用res.status(404)设置状态码
app.use((req, res) => {
  // 响应的方式有两种

  // 方式一：返回json数据给前端工程师
  // res.status(404); // 设置状态码
  // res.send({
  //   status: 404,
  //   code: 'not found',
  // });

  // 方式二：返回html页给用户看
  res.status(404); // 设置状态码
  let pathUrl = path.join(__dirname, './views/404.html');
  res.sendFile(pathUrl);
});
