const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 如果我们响应提示不用p标签了，而改成span标签，改的工作量就比较大
// 如果使用统一逻辑错误处理，就只需要改一处即可
app.get('/register', (req, res, next) => {
  let { username, password } = req.query;
  // console.log(username, password);
  // 1、非空判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 2、注册逻辑判断(如果是admin，就说用户名已占用，否则可以注册)
  if (username === 'admin') {
    next('用户名已占用，请换一个');
  } else {
    res.send({
      status: 200,
      code: '注册成功',
    });
  }
});

app.use((err, req, res, next) => {
  // console.log(err);
  res.status(500);
  res.send({
    status: 500,
    code: err,
  });
});
