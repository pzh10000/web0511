const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 如果我们响应提示不用p标签了，而改成span标签，改的工作量就比较大
app.get('/register', (req, res) => {
  let { username, password } = req.query;
  // console.log(username, password);
  // 1、非空判断
  if (!username || !password) {
    res.send('<p>用户名和密码必须填写</p>');
    return;
  }

  // 2、注册逻辑判断(如果是admin，就说用户名已占用，否则可以注册)
  if (username === 'admin') {
    res.send('<p>用户名已占用，请换一个</p>');
  } else {
    res.send('<h1>注册成功</h1>');
  }
});
