const fs = require('fs'); // 必须先引入fs内置模块
// console.log(fs); // 它是一个对象，它下面有很多操作文件的方法

// 打开文件 -> 对文件进行操作 -> 关闭文件（释放内存）

// 1、打开文件：返回文件引用
// fs.openSync(path[, flags])    常用的模式：只读r(默认)、写入w、追加a
let file = fs.openSync('./demo/abc.txt', 'a');

// 2、对文件进行操作
// fs.writeSync( 文件的引用, 内容 )
fs.writeSync(file, 'abc');

// 3、关闭文件（释放内存）
// fs.closeSync(文件的引用)
fs.closeSync(file);
