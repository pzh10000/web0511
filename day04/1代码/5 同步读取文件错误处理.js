const fs = require('fs');

// try {
//   执行可能有错误的代码
// } catch (error) {
//     捕获错误，代码继续向下执行
// );

console.log(1);

try {
  // 执行可能报错的代码，如果有错误，执行catch
  let str = fs.readFileSync('./demo/abc1.txt', 'utf-8');
  console.log(str);
} catch (error) {
  // error则为错误对象
  console.log(error);
}

console.log(2);
