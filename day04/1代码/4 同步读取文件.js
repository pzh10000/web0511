const fs = require('fs');

// fs.readFileSync( path[,options] )
// 返回文件的内容

// 没有第二个参数，默认以16进制读取，需要调用toString()转换成字符串
let str = fs.readFileSync('./demo/abc.txt');
console.log(str); // <Buffer 68 65 6c 6c 6f>
console.log(str.toString()); // hello

// 有第二个参数
// let str = fs.readFileSync('./demo/abc.txt', { encoding: 'utf-8' });
let str = fs.readFileSync('./demo/abc.txt', 'utf-8');
console.log(str);
