const fs = require('fs');

// fs.readFile( path [,options], callback )
// 结果必须用回调函数接收。回调函数的第一个参数是错误对象，第二个参数是读取的数据

// 没有用第二个参数
fs.readFile('./demo/abc.txt', (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
  console.log(result.toString());
});

// -------------------
// 有第二个参数
fs.readFile('./demo/abc.txt', { encoding: 'utf-8' }, (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
});

fs.readFile('./demo/abc.txt', 'utf-8', (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
});

// ---------------------
// 读取错误
fs.readFile('./demo/abc1.txt', (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
  console.log(result.toString());
});
