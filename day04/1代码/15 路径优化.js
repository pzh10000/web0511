const fs = require('fs');

// fs操作的文件，如果使用相对路径（是相对于命令行的路径）
// let str = fs.readFileSync('./demo/c.txt', 'utf-8');
// console.log(str);

// --------------------

// 解决：使用绝对路径
let path = __dirname + '\\demo\\c.txt'; // 拼出一个绝对的地址

let str = fs.readFileSync(path, 'utf-8');
console.log(str);
