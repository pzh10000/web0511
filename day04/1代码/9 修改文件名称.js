const fs = require('fs');

// fs.renameSync( oldpath, newpath )

// 1、改名
// fs.renameSync('./demo/abc.txt', './demo/tianzhu.txt');

// 2、移动文件
fs.renameSync('./demo/tianzhu.txt', './demo2/abc.txt');
