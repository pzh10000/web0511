const fs = require('fs');

// fs.rmdirSync( path[,options] )
// fs.rmdirSync('./demo2'); // 删除一个空的文件夹

fs.rmdirSync('./demo3', { recursive: true }); // 递归删除，node的14以上版本支持
