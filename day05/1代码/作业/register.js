const fs = require('fs');

let data = fs.readFileSync('./persons.json', 'utf-8');
data = JSON.parse(data); // 注册过的数据
// console.log(data);

let user = { username: 'admin1', password: '12345' };

let o = data.findIndex((item) => item.username === user.username);
if (o === -1) {
  // 可以注册
  data.push(user);
  fs.writeFileSync('./persons.json', JSON.stringify(data));
  console.log('注册成功');
} else {
  // 用户名已占用
  console.log('用户名已占用');
}
