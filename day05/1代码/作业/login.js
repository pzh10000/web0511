const fs = require('fs');

let data = fs.readFileSync('./persons.json', 'utf-8');
data = JSON.parse(data); // 注册过的数据

let user = { username: 'admin1', password: '123456' };

let o = data.find(
  (item) => item.username === user.username && item.password === user.password
);

if (o) {
  console.log('登录成功');
} else {
  console.log('用户名和密码不匹配');
}
