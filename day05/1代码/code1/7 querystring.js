const qs = require('querystring');

// 操作查询了符串，可以将对象转成查询字符串，也可以将查询字符串转成对象
let obj = {
  name: 'zs',
  age: 3,
  sex: 'man',
};

// 将对象转成查询字符串
let str = qs.stringify(obj);
console.log(str); // 'name=zs&age=3&sex=man'

// 将查询字符串转成对象
let o = qs.parse(str);
console.log(o); // { name: 'zs', age: '3', sex: 'man' }
