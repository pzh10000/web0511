// 接收一个对象
// let obj = require('./module_demo/m1.js');
// console.log(obj); // { uname: '小赵', age: 3, fn: [Function: fn] }

// ----------------------
// 解构数据
let { uname, age, fn } = require('./module_demo/m1.js');
console.log(uname, age);
fn();
