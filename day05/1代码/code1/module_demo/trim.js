function Trim(str) {
  let re = /^\s+|\s+$/g;
  return str.replace(re, '');
}

Trim.left = function (str) {
  let re = /^\s+/;
  return str.replace(re, '');
};

Trim.right = function (str) {
  let re = /\s+$/;
  return str.replace(re, '');
};

module.exports = Trim;
