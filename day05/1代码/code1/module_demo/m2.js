let uname = '小赵';
let age = 3;
let fn = function () {
  console.log('我是前端开发');
};

// 使用exports暴露数据
// exports就是module.exports的引用，但是默认暴露的是module.exports
// 只有这一种方式
exports.uname = uname;
exports.age = age;
exports.fn = fn;
