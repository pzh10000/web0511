let uname = '小赵';
let age = 3;
let fn = function () {
  console.log('我是前端开发');
};

// 暴露数据
// module.exports它是一个对象，可以给它加属性，也可以给它赋值，暴露出去的是module.exports这个对象

// 暴露方式一
// module.exports.uname = uname;
// module.exports.age = age;
// module.exports.fn = fn;

// ---------------------
// 暴露方式二
module.exports = {
  uname,
  age,
  fn,
};
