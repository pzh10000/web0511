// 作用：解析网址
// 注意：这个模块比较特殊，不需要引入（但需要new实例化，将地址做为参数传进去），类似于全局变量的用法

const myurl = 'http://www.ujiuye.com:8080/a/b/c?name=zs&age=3#ab';
let obj = new URL(myurl);
console.log(obj); // 返回一个对象，这个对象是对路径的解析
console.log(obj.port);
