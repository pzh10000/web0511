const path = require('path');

// console.log(path); // 它是一个对象，它下面有很多的属性和方法

// 路径
const filepath = 'a/b/c/d/a.html';

// parse
// let o = path.parse(filepath);
// console.log(o); // { root: '', dir: 'a/b/c/d', base: 'a.html', ext: '.html', name: 'a' }

// basename和extname
// console.log(path.basename(filepath)); // 完整的文件名称
// console.log(path.extname(filepath)); // 文件扩展名称

// join
// 拼接文件路径(最常用，只要用到fs，就要用到path.join())
let ab = path.join(__dirname, './module_demo/m1.js');
console.log(ab);

// let b = __dirname + '\\module_demo\\m1.js'; // 原来的手动拼接就不用了
// console.log(b);
