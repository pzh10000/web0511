// 1、引入express
const express = require('express');
// 2、创建服务器
let app = express();
// 3、监听端口
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 如果运行http://localhost:3000/  会跑到这个函数中
app.get('/', (req, res) => {
  res.send('我是首页'); // 返回字符串
});

// 如果运行http://localhost:3000/demo  会跑到这个函数中
app.get('/demo', (req, res) => {
  res.send('<h1>我是一个demo</h1>'); // 返回html结构
});

// 如果运行http://localhost:3000/test  会跑到这个函数中
app.get('/test', (req, res) => {
  res.send({ name: 'zs', age: 3 }); // 返回对象
});
