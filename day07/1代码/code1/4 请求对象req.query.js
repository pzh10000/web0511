const express = require('express');
let app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.get('/demo', (req, res) => {
  // req.query
  // 作用：获取url地址 ? 后面的查询字符串参数，它是一个对象
  console.log(req.query); // { a: '1', b: '2', c: '3' }

  res.send(req.query);
});
