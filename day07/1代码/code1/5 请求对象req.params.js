const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// app.get('/subject/35198422', (req, res) => {
//   res.send('出拳吧，妈妈');
// });

// app.get('/subject/30165311', (req, res) => {
//   res.send('坏蛋联盟');
// });

// app.get('/subject/26147418', (req, res) => {
//   res.send('神奇动物');
// });

// ---------------------
// 模拟豆瓣的动态路径
let arr = [
  { id: 23, msg: '这是我的兄弟' },
  { id: 112, msg: '今天喝什么' },
  { id: 44, msg: '明天不出去了' },
  { id: 5, msg: '就在这里了' },
  { id: 78, msg: '论牛肉的做法' },
];

app.get('/subject/:id', (req, res) => {
  // console.log(req.params); // 获取动态路由，它是一个对象
  let { id } = req.params; // 实际开发，取得这个ID，去数据库中查
  // console.log(id);

  let o = arr.find((item) => item.id == id);
  if (o) {
    res.send(o);
  } else {
    res.send('你输入的动态路由参数有误，看三胖的');
  }
});

// http://localhost:3000/demo/12?a=1&b=2
// 12就是动态路由
// a=1&b=2就是查询字符串参数
app.get('/demo/:id', (req, res) => {
  console.log(req.query); // 查询字串参数
  console.log(req.params); // 动态路由参数

  res.send('即有动态路由传参，也有查询字符串参数');
});
