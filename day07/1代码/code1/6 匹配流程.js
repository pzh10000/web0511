const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 学员系统
app.get('/student', (req, res) => {
  res.send('查找一个学生');
});

app.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

app.post('/student', (req, res) => {
  res.send('增加一个学生');
});

app.put('/student', (req, res) => {
  res.send('更新一个学生');
});

// 有相同的路径和方法，永远不会匹配到这里
app.get('/student', (req, res) => {
  res.send('查找一个学生2222');
});
