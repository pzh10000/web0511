const express = require('express');
let app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const fs = require('fs');
const path = require('path');

// 通常情况，可以响应多种数据结构
app.get('/demo', (req, res) => {
  // 1、返回的数据类型更加灵活，可以是string、Object
  // 2、根据数据类型灵活的设置Content-type( 主要是html和json )，并且默认就是utf-8
  // 2.1：普通的字符当作 html 渲染
  // 2.2 :  返回的是Object，则 application/json 渲染

  // res.send('你好啊小老弟');
  // res.send({ name: '小赵', age: 3 });
  res.send('<h1>隔壁老赵</h1>');
  // res.send('abc'); // 一个请求，不可以有两次响应（有请求就会有响应，且一次请求只有一次响应）
});

// 响应图片
// 演示可以设置不同的Content-type。图片的展示不用这样
app.get('/img', (req, res) => {
  let url = path.join(__dirname, './img/abc.jpg');
  let data = fs.readFileSync(url); // 读取文件的二进制
  // console.log(data);

  res.setHeader('Content-type', 'image/jpeg');
  res.send(data);
});

// 响应文件：主要用于响应html文件
app.get('/file', (req, res) => {
  // res.sendFile(绝对地址); // 关键，参数是文件的绝对地址

  // let url = path.join(__dirname, './img/abc.jpg'); // 图片也不这样用
  // res.sendFile(url);

  // 响应html文件
  res.sendFile(path.join(__dirname, './views/1form表单发起get和post请求.html'));
});
