const http = require('http');
const qs = require('querystring');
const fs = require('fs');
const path = require('path');

let app = http.createServer();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' });

  let url = req.url; // /login?username=zs&password=123

  if (url.startsWith('/login')) {
    // 登录
    // 1、获取用户输入的信息
    let user = qs.parse(url.split('?')[1]); // 用户输入的用户名和密码
    // console.log(user);
    if (!user.username || !user.password) {
      res.end('用户名和密码必须填写');
      return;
    }

    // 2、获取文件中的数据
    let filePath = path.join(__dirname, './data/persons.json');
    let data = fs.readFileSync(filePath, 'utf-8');
    data = JSON.parse(data);

    // 3、登录判断
    let o = data.find(
      (item) =>
        item.username === user.username && item.password === user.password
    );
    if (o) {
      // 登录成功
      res.end('登录成功');
    } else {
      // 登录失败
      res.end('用户名和密码不匹配');
    }
  } else if (url.startsWith('/register')) {
    // 注册
    // 1、获取用户输入
    let user = qs.parse(url.split('?')[1]); // 用户输入的用户名和密码
    // console.log(user);
    if (!user.username || !user.password) {
      res.end('用户名和密码必须填写');
      return;
    }

    // 2、获取文件中的数据
    let filePath = path.join(__dirname, './data/persons.json');
    let data = fs.readFileSync(filePath, 'utf-8');
    data = JSON.parse(data);

    // 3、注册逻辑
    let o = data.findIndex((item) => item.username === user.username);
    if (o === -1) {
      // 可以注册
      data.push(user); // 将用户的信息添加到注册文件中

      fs.writeFileSync(filePath, JSON.stringify(data)); // 再写入到json中
      res.end('注册成功');
    } else {
      // 用户名已占用
      res.end('此用户名太火');
    }
  } else {
    // 404
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf8' });
    res.end('404');
  }
});
