const express = require('express'); // 引入
let router = express.Router(); // 创建路由模块

// 学生
router.get('/student', (req, res) => {
  res.send('查找一个学生');
});

router.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

router.post('/student', (req, res) => {
  res.send('增加一个学生');
});

router.put('/student', (req, res) => {
  res.send('修改一个学生');
});

module.exports = router;
