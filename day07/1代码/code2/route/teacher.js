const express = require('express');
let router = express.Router();

// 老师
router.get('/teacher', (req, res) => {
  res.send('查找一个老师');
});

router.delete('/teacher', (req, res) => {
  res.send('删除一个老师');
});

router.post('/teacher', (req, res) => {
  res.send('增加一个老师');
});

router.put('/teacher', (req, res) => {
  res.send('修改一个老师');
});

module.exports = router;
