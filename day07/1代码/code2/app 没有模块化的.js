const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 学生
app.get('/student', (req, res) => {
  res.send('查找一个学生');
});

app.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

app.post('/student', (req, res) => {
  res.send('增加一个学生');
});

app.put('/student', (req, res) => {
  res.send('修改一个学生');
});

// 老师
app.get('/teacher', (req, res) => {
  res.send('查找一个老师');
});

app.delete('/teacher', (req, res) => {
  res.send('删除一个老师');
});

app.post('/teacher', (req, res) => {
  res.send('增加一个老师');
});

app.put('/teacher', (req, res) => {
  res.send('修改一个老师');
});

// 考试

// 教室

// 课表
